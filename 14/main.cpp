#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter you name: ";
	std::string name;
	std::getline(std::cin, name);

	std::cout << "Enter you age: ";
	std::string age;
	std::getline(std::cin, age);
	std::cout << name << " " << age << "\n";

	std::cout << name.length() << "\n";
	std::cout << age.length() << "\n";
	std::cout << name[0] << "\n";
	std::cout << name[name.size() - 1] << "\n";
}